FROM maven:alpine 
ADD dist/ /app/
WORKDIR /app/
EXPOSE 8000
ENTRYPOINT java -cp  jb-hello-world-maven-0.1.0.jar  hello.HelloWorld
